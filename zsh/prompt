source ${SH_LIBRARY_PATH}/colors
source ${SH_LIBRARY_PATH}/color_themes

# Shorten a one-lined text
# Typicall use: shorten the path or branch name, for the prompt
function shorten()
{
  local text="$*"
  readonly threshold=20
  readonly infinite=99 # TODO Stupid hack, just to eat up till the end of the zsh array

  if [ ${#text} -le ${threshold} ]; then
    # nothing to do
    echo -n "${text}"
  elif [[ "${text}" =~ "^(/.+){2,}" ]]; then
    # path-like text --> compress the beginning part
    local tokens=("${(s:/:)text}") # split into array
    local shorten_text=""
    for t in "${tokens[@]}"; do
      [ -z "${t}" ] && continue # skip the first / token, or occurrence of // tokens
      shorten_text+="/"
      shorten_text+="${t[1]}" # only the first char
    done
    shorten_text+="${${tokens[-1]}[2,${infinite}]}" # complete the last token in full
    echo -n "${shorten_text}"
  elif [[ "${text}" =~ "^~/.*" ]]; then
    # path-like text, with a ~ component --> compress the beginning part
    transformed_text="${text[2,${infinite}]}" # drop the ~
    shortened_text=$(shorten "${transformed_text}")
    echo -n "~${shortened_text}" # put back the ~
  elif [[ "${text}" =~ "^~.+(/.+)+" ]]; then
    # path-like text, with a ~xxx component --> compress the beginning part
    transformed_text="/${text[2,${infinite}]}" # replace ~ by a /
    shortened_text=$(shorten "${transformed_text}")
    echo -n "~${shortened_text[2,${infinite}]}" # put back the ~
  else
    # fallback --> put ellipsis in the middle
    readonly chunk_len=5
    echo -n "${text[1,${chunk_len}]}…${text[-${chunk_len},${infinite}]}"
  fi
}

source ${SH_LIBRARY_PATH}/git-prompt
source ${SH_LIBRARY_PATH}/zsh-async/async.zsh; async_init

readonly PROMPT_CHAR='>'

readonly PROMPT_COLOR1="%{$(color_compose_term $(color_theme_accent3_fg 0) $(color_theme_accent3_bg 0))%}"
readonly PROMPT_COLOR2="%{$(color_compose_term $(color_theme_accent3_fg 1) $(color_theme_accent3_bg 1))%}"
readonly PROMPT_COLOR3="%{$(color_compose_term $(color_theme_accent3_fg 2) $(color_theme_accent3_bg 2))%}"

readonly PS_COLOR_RESET="%{$(echo -ne ${color_reset})%}"

function rprompt_ready()
{
  local result="$3" # $3 is resulting (stdout) output from async job execution
  RPROMPT="${result}${PS_COLOR_RESET}"
  zle reset-prompt
}
async_start_worker rprompt_compose -n
async_register_callback rprompt_compose rprompt_ready
 
function prompt_compose()
{
  PROMPT="${PROMPT_COLOR1}%n "
  # Show hostname only on remote
  if [ -n "$SSH_CLIENT" -o -n "$SSH_TTY" ]; then
    PROMPT+="${PROMPT_COLOR2} %M "
  fi
  PROMPT+="${PROMPT_COLOR3} $(shorten $(pwd | sed "s|${HOME}|~|")) "
  PROMPT+="${PS_COLOR_RESET}${PROMPT_CHAR} "

  RPROMPT="" # empty for now, will be filled up inside the rprompt_ready callback
  async_job rprompt_compose git_prompt "$(pwd)"
}

precmd_functions+=(prompt_compose)
prompt_compose
